package com.example.online_school.instructors;

import com.example.online_school.instructors.models.Instructor;
import com.example.online_school.instructors.models.InstructorDto;
import com.example.online_school.instructors.utils.InstructorMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class InstructorServiceImpl implements InstructorService{

    private InstructorRepository instructorRepository;
    private InstructorMapper instructorMapper;

    @Autowired
    public InstructorServiceImpl(InstructorRepository instructorRepository, InstructorMapper instructorMapper) {
        this.instructorRepository = instructorRepository;
        this.instructorMapper = instructorMapper;
    }

    @Override
    public Iterable<Instructor> findAll() {
        return instructorRepository.findAll();
    }

    @Override
    public Optional<Instructor> findById(Long id) {
        Optional<Instructor> optionalInstructor = instructorRepository.findById(id);
        if (optionalInstructor.isPresent()) {
            return optionalInstructor;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "the instructor with id " + id + " was not found");
        }
    }

    @Override
    public Instructor save(Instructor instructor) {
        return instructorRepository.save(instructor);
    }

    @Override
    public Instructor update(Long id, InstructorDto instructorDto) {
        Optional<Instructor> optionalInstructor = instructorRepository.findById(id);
        if (optionalInstructor.isPresent()) {
            Instructor myInstructor = optionalInstructor.get();
            instructorMapper.updateInstructorFromDto(instructorDto, myInstructor);
            return instructorRepository.save(myInstructor);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "the instructor with id " + id + " was not found");
        }
    }

    @Override
    public void delete(Instructor instructor) {
        instructorRepository.delete(instructor);
    }

    @Override
    public List<Instructor> findByIdIn(List<Long> ids) {
        return instructorRepository.findByIdIn(ids);
    }
}
