package com.example.online_school.instructors.utils;

import com.example.online_school.instructors.models.Instructor;
import com.example.online_school.instructors.models.InstructorDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface InstructorMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateInstructorFromDto(InstructorDto dto, @MappingTarget Instructor instructor);
}
