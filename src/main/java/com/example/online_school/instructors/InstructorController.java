package com.example.online_school.instructors;

import com.example.online_school.groups.models.Group;
import com.example.online_school.instructors.models.Instructor;
import com.example.online_school.instructors.models.InstructorDto;
import com.example.online_school.trainings.models.Training;
import com.example.online_school.trainingsGroups.TrainingGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/instructors")
public class InstructorController {

    private InstructorService instructorService;
    private TrainingGroupService trainingGroupService;

    @Autowired
    public InstructorController(InstructorService instructorService, TrainingGroupService trainingGroupService) {
        this.instructorService = instructorService;
        this.trainingGroupService = trainingGroupService;
    }

    @GetMapping
    public Iterable<Instructor> findAll() {
        return instructorService.findAll();
    }

    @GetMapping(path = "/{id}")
    public Optional<Instructor> findById(@PathVariable Long id) {
        return instructorService.findById(id);
    }

    @PostMapping
    public Instructor save(@RequestBody Instructor instructor) {
        return instructorService.save(instructor);
    }

    @PatchMapping(path = "/{id}")
    public Instructor update(@PathVariable Long id, @RequestBody InstructorDto instructorDto) {
        return instructorService.update(id, instructorDto);
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable ("id") Instructor instructor) {
        instructorService.delete(instructor);
    }

    @GetMapping(path = "/{id}/groups")
    public List<Group> getGroupsByInstructor(@PathVariable Long id) {
        return trainingGroupService.getGroupsByInstructor(id);
    }

    @GetMapping(path = "/{id}/trainings")
    public List<Training> getTrainingsByInstructor(@PathVariable Long id) {
        return trainingGroupService.getTrainingsByInstructor(id);
    }
}
