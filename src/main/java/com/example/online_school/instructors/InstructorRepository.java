package com.example.online_school.instructors;

import com.example.online_school.instructors.models.Instructor;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface InstructorRepository extends CrudRepository<Instructor, Long> {

    List<Instructor> findByIdIn(List<Long> ids);
}
