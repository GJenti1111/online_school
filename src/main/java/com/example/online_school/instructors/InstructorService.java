package com.example.online_school.instructors;

import com.example.online_school.instructors.models.Instructor;
import com.example.online_school.instructors.models.InstructorDto;

import java.util.List;
import java.util.Optional;

public interface InstructorService {

    Iterable<Instructor> findAll();

    Optional<Instructor> findById(Long id);

    Instructor save(Instructor instructor);

    Instructor update(Long id, InstructorDto instructorDto);

    void delete(Instructor instructor);

    List<Instructor> findByIdIn(List<Long> ids);
}
