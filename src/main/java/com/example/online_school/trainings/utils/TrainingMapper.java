package com.example.online_school.trainings.utils;

import com.example.online_school.trainings.models.Training;
import com.example.online_school.trainings.models.TrainingDto;
import org.mapstruct.BeanMapping;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(componentModel = "spring")
public interface TrainingMapper {

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
    void updateTrainingFromDto(TrainingDto dto, @MappingTarget Training training);
}
