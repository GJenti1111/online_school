package com.example.online_school.trainings;

import com.example.online_school.trainings.models.Training;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TrainingRepository extends CrudRepository<Training, Long> {

    List<Training> findByIdIn(List<Long> ids);
}
