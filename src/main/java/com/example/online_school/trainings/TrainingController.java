package com.example.online_school.trainings;

import com.example.online_school.groups.models.Group;
import com.example.online_school.instructors.models.Instructor;
import com.example.online_school.trainings.models.Training;
import com.example.online_school.trainings.models.TrainingDto;
import com.example.online_school.trainingsGroups.TrainingGroupService;
import com.example.online_school.trainingsGroups.models.TrainingGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/trainings")
public class TrainingController {

    private TrainingService trainingService;
    private TrainingGroupService trainingGroupService;

    @Autowired
    public TrainingController(TrainingService trainingService, TrainingGroupService trainingGroupService) {
        this.trainingService = trainingService;
        this.trainingGroupService = trainingGroupService;
    }

    @GetMapping
    public Iterable<Training> findAll() {
        return trainingService.findAll();
    }

    @GetMapping(path = "/{id}")
    public Optional<Training> findById(@PathVariable Long id) {
        return trainingService.findById(id);
    }

    @PostMapping
    public Training save(@RequestBody Training training) {
        return trainingService.save(training);
    }

    @PatchMapping(path = "/{id}")
    public Training update(@PathVariable Long id, @RequestBody TrainingDto trainingDto) {
        return trainingService.update(id, trainingDto);
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable ("id") Training training) {
        trainingService.delete(training);
    }

    @GetMapping(path = "/{id}/groups")
    List<Group> getGroupsByTraining(@PathVariable ("id") Long id) {
        return trainingGroupService.getGroupsByTraining(id);
    }

    @GetMapping(path = "/{id}/instructors")
    List<Instructor> getInstructorsByTraining(@PathVariable Long id) {
        return trainingGroupService.getInstructorsByTraining(id);
    }

    @PostMapping(path = "/{trainingId}/groups/{groupId}/instructors/{instructorId}")
    public TrainingGroup save(@PathVariable Long trainingId, @PathVariable Long groupId, @PathVariable Long instructorId) {
        return trainingGroupService.save(trainingId, groupId, instructorId);
    }
}
