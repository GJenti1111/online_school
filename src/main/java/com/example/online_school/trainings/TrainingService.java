package com.example.online_school.trainings;

import com.example.online_school.trainings.models.Training;
import com.example.online_school.trainings.models.TrainingDto;

import java.util.List;
import java.util.Optional;

public interface TrainingService {

    Iterable<Training> findAll();

    Optional<Training> findById(Long id);

    Training save(Training training);

    Training update(Long id, TrainingDto trainingDto);

    void delete(Training training);

    List<Training> findByIdIn(List<Long> ids);
}
