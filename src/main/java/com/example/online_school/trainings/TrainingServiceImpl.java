package com.example.online_school.trainings;

import com.example.online_school.trainings.models.Training;
import com.example.online_school.trainings.models.TrainingDto;
import com.example.online_school.trainings.utils.TrainingMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class TrainingServiceImpl implements TrainingService {

    private TrainingRepository trainingRepository;
    private TrainingMapper trainingMapper;

    @Autowired
    public TrainingServiceImpl(TrainingRepository trainingRepository, TrainingMapper trainingMapper) {
        this.trainingRepository = trainingRepository;
        this.trainingMapper = trainingMapper;
    }

    @Override
    public Iterable<Training> findAll() {
        return trainingRepository.findAll();
    }

    @Override
    public Optional<Training> findById(Long id) {
        Optional<Training> optionalTraining = trainingRepository.findById(id);
        if (optionalTraining.isPresent()) {
            return optionalTraining;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "the training with id " + id + "is not found ");
        }
    }

    @Override
    public Training save(Training training) {
        return trainingRepository.save(training);
    }

    @Override
    public Training update(Long id, TrainingDto trainingDto) {
        Optional<Training> optionalTraining = trainingRepository.findById(id);
        if (optionalTraining.isPresent()) {
            Training myTraining = optionalTraining.get();
            trainingMapper.updateTrainingFromDto(trainingDto, myTraining);
            return trainingRepository.save(myTraining);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "the training with id " + id + "is not found ");
        }
    }

    @Override
    public void delete(Training training) {
        trainingRepository.delete(training);
    }

    @Override
    public List<Training> findByIdIn(List<Long> ids) {
        return trainingRepository.findByIdIn(ids);
    }
}
