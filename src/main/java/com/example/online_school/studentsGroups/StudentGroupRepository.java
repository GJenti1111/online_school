package com.example.online_school.studentsGroups;

import com.example.online_school.students.models.Student;
import com.example.online_school.studentsGroups.models.StudentGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface StudentGroupRepository extends CrudRepository<StudentGroup, Long> {

    @Query(value = "SELECT student_id from students_groups where group_id = :id " ,nativeQuery = true)
    List<Long> getStudentByGroup(@PathVariable("id") Long id);

    @Query(value = "SELECT group_id from students_groups where student_id = :id ",nativeQuery = true)
    List<Long> getGroupByStudent(@PathVariable("id") Long id);

    @Query(value = "SELECT * from students_groups WHERE group_id = :groupId and student_id = :studentId", nativeQuery = true)
    StudentGroup getSGByBothIds(@PathVariable Long groupId, @PathVariable Long studentId);

    @Query(value = "DELETE from students_groups WHERE group_id = :groupId and student_id = :studentId", nativeQuery = true)
    void deleteStudentGroupByBothIds(@PathVariable Long groupId, @PathVariable Long studentId);

    @Override
    void delete(StudentGroup entity);
}
