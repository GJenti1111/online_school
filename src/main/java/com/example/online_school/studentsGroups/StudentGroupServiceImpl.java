package com.example.online_school.studentsGroups;

import com.example.online_school.groups.GroupService;
import com.example.online_school.groups.models.Group;
import com.example.online_school.students.StudentService;
import com.example.online_school.students.models.Student;
import com.example.online_school.studentsGroups.models.StudentGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class StudentGroupServiceImpl implements StudentGroupService{

    private StudentGroupRepository studentGroupRepository;
    private StudentService studentService;
    private GroupService groupService;

    @Autowired
    public StudentGroupServiceImpl(StudentGroupRepository studentGroupRepository, StudentService studentService, GroupService groupService) {
        this.studentGroupRepository = studentGroupRepository;
        this.studentService = studentService;
        this.groupService = groupService;
    }

    @Override
    public StudentGroup save(Long groupId, Long studentId) {

        StudentGroup studentGroup = new StudentGroup();

        Student student = studentService.findById(studentId).get();
        Group group = groupService.findById(groupId).get();
        Timestamp currentTime = Timestamp.from(Instant.now());

        studentGroup.setStudent(student);
        studentGroup.setGroup(group);
        studentGroup.setCreatedAt(currentTime);
        studentGroup.setUpdatedAt(currentTime);

        return studentGroupRepository.save(studentGroup);

    }

    @Override
    public List<Student> getStudentByGroup(Long id) {

        List<Long> studentIds = studentGroupRepository.getStudentByGroup(id);
        return studentService.findByIdIn(studentIds);
    }

    @Override
    public List<Group> getGroupByStudent(Long id) {
        List<Long> groupIds = studentGroupRepository.getGroupByStudent(id);
        return groupService.findByIdIn(groupIds);
    }

    @Override
    public StudentGroup getSGByBothIds(Long groupId, Long studentId) {
        return studentGroupRepository.getSGByBothIds(groupId, studentId);
    }

    @Override
    public void deleteStudentGroupByBothIds(Long groupId, Long studentId) {
        StudentGroup studentGroup = this.getSGByBothIds(groupId,studentId);
        studentGroupRepository.delete(studentGroup);
    }

    @Override
    public Iterable<StudentGroup> saveAll(Long groupId, List<Long> ids) {

        List<StudentGroup> studentGroupList = new ArrayList<>();
        List<Student> students = studentService.findByIdIn(ids);
        Group group = groupService.findById(groupId).get();
        Timestamp currentTime = Timestamp.from(Instant.now());

        for (Student student: students) {
            StudentGroup studentGroup = new StudentGroup();
            studentGroup.setGroup(group);
            studentGroup.setStudent(student);
            studentGroup.setCreatedAt(currentTime);
            studentGroup.setUpdatedAt(currentTime);
            studentGroupList.add(studentGroup);
        }

        return studentGroupRepository.saveAll(studentGroupList);
    }
}
