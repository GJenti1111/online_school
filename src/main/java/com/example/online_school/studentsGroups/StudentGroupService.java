package com.example.online_school.studentsGroups;

import com.example.online_school.groups.models.Group;
import com.example.online_school.students.models.Student;
import com.example.online_school.studentsGroups.models.StudentGroup;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface StudentGroupService {

    StudentGroup save(Long groupId, Long studentId);

    List<Student> getStudentByGroup(@PathVariable("id") Long id);

    List<Group> getGroupByStudent(@PathVariable("id") Long id);

    StudentGroup getSGByBothIds(@PathVariable Long groupId, @PathVariable Long studentId);

    void deleteStudentGroupByBothIds(@PathVariable Long groupId,@PathVariable Long studentId);

    Iterable<StudentGroup> saveAll( Long groupId, List<Long> ids);
}
