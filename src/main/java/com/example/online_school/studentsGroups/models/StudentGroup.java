package com.example.online_school.studentsGroups.models;

import com.example.online_school.commons.BaseModel;
import com.example.online_school.groups.models.Group;
import com.example.online_school.students.models.Student;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "students_groups")
public class StudentGroup extends BaseModel {

    @ManyToOne
    @JoinColumn(name = "student_id")
    Student student;

    @ManyToOne
    @JoinColumn(name = "group_id")
    Group group;

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
}
