package com.example.online_school.students;

import com.example.online_school.students.models.Student;
import com.example.online_school.students.models.StudentDto;
import com.example.online_school.students.utils.StudentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService{

    StudentRepository studentRepository;
    StudentMapper studentMapper;

    @Autowired
    public StudentServiceImpl(StudentRepository studentRepository, StudentMapper studentMapper) {
        this.studentRepository = studentRepository;
        this.studentMapper = studentMapper;
    }

    @Override
    public Iterable<Student> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public Optional<Student> findById(Long id) {
        Optional<Student> optionalStudent = studentRepository.findById(id);
        if (optionalStudent.isPresent()) {
            return optionalStudent;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The student with id " + id + " not found");
        }
    }

    @Override
    public Student save(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Student update(Long id, StudentDto studentDto) {
        Optional<Student> optionalStudent = studentRepository.findById(id);
        if (optionalStudent.isPresent()) {
            Student mystudent = optionalStudent.get();
            studentMapper.updateStudentFromDto(studentDto, mystudent);
            return studentRepository.save(mystudent);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "The student with id " + id + " not found");
        }
    }

    @Override
    public void delete(Student student) {
        studentRepository.delete(student);
    }

    @Override
    public List<Student> findByIdIn(List<Long> ids) {
        return studentRepository.findByIdIn(ids);
    }
}
