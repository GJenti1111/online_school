package com.example.online_school.students;

import com.example.online_school.students.models.Student;
import com.example.online_school.students.models.StudentDto;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    Iterable<Student> findAll();

    Optional<Student> findById(Long id);

    Student save(Student student);

    Student update(Long id, StudentDto studentDto);

    void delete(Student student);

    List<Student> findByIdIn(List<Long> ids);
}
