package com.example.online_school.students;

import com.example.online_school.students.models.Student;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface StudentRepository extends CrudRepository<Student, Long> {

    List<Student> findByIdIn(List<Long> ids);
}
