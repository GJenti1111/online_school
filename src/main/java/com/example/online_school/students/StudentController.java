package com.example.online_school.students;

import com.example.online_school.groups.models.Group;
import com.example.online_school.students.models.Student;
import com.example.online_school.students.models.StudentDto;
import com.example.online_school.studentsGroups.StudentGroupService;
import com.example.online_school.studentsGroups.models.StudentGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/students")
public class StudentController {

    private StudentService studentService;
    private StudentGroupService studentGroupService;

    @Autowired
    public StudentController(StudentService studentService, StudentGroupService studentGroupService) {
        this.studentService = studentService;
        this.studentGroupService = studentGroupService;
    }

    @GetMapping
    public Iterable<Student> findAll() {
        return studentService.findAll();
    }

    @GetMapping(path = "/{id}")
    public Optional<Student> findById(@PathVariable Long id) {
        return studentService.findById(id);
    }

    @PostMapping
    public Student save(@RequestBody Student student) {
        return studentService.save(student);
    }

    @PatchMapping(path = "/{id}")
    public Student update(@PathVariable Long id, @RequestBody StudentDto studentDto) {
        return studentService.update(id, studentDto);
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable("id") Student student) {
        studentService.delete(student);
    }

    @GetMapping(path = "/{id}/groups")
    public List<Group> getGroupByStudent(@PathVariable("id") Long id) {
        return studentGroupService.getGroupByStudent(id);
    }

    @GetMapping(path = "/{studentId}/groups/{groupId}")
    public StudentGroup getSGByBothIds(@PathVariable Long groupId, @PathVariable Long studentId) {
        return studentGroupService.getSGByBothIds(groupId, studentId);
    }

    @DeleteMapping(path = "/{studentId}/groups/{groupId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteStudentGroupByBothIds(@PathVariable Long groupId, @PathVariable Long studentId) {
        studentGroupService.deleteStudentGroupByBothIds(groupId, studentId);
    }
}
