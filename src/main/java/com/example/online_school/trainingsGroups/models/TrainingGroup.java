package com.example.online_school.trainingsGroups.models;

import com.example.online_school.commons.BaseModel;
import com.example.online_school.groups.models.Group;
import com.example.online_school.instructors.models.Instructor;
import com.example.online_school.trainings.models.Training;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "trainings_groups")
public class TrainingGroup extends BaseModel {

    @ManyToOne
    @JoinColumn(name = "group_id")
    Group group;

    @ManyToOne
    @JoinColumn(name = "instructor_id")
    Instructor instructor;

    @ManyToOne
    @JoinColumn(name = "training_id")
    Training training;

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public Instructor getInstructor() {
        return instructor;
    }

    public void setInstructor(Instructor instructor) {
        this.instructor = instructor;
    }

    public Training getTraining() {
        return training;
    }

    public void setTraining(Training training) {
        this.training = training;
    }
}
