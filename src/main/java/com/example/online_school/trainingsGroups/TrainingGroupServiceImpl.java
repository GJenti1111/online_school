package com.example.online_school.trainingsGroups;

import com.example.online_school.groups.GroupService;
import com.example.online_school.groups.models.Group;
import com.example.online_school.instructors.InstructorService;
import com.example.online_school.instructors.models.Instructor;
import com.example.online_school.trainings.TrainingService;
import com.example.online_school.trainings.models.Training;
import com.example.online_school.trainingsGroups.models.TrainingGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class TrainingGroupServiceImpl implements TrainingGroupService {

    private TrainingGroupRepository trainingGroupRepository;
    private TrainingService trainingService;
    private GroupService groupService;
    private InstructorService instructorService;

    @Autowired
    public TrainingGroupServiceImpl(TrainingGroupRepository trainingGroupRepository, TrainingService trainingService, GroupService groupService, InstructorService instructorService) {
        this.trainingGroupRepository = trainingGroupRepository;
        this.trainingService = trainingService;
        this.groupService = groupService;
        this.instructorService = instructorService;
    }

    @Override
    public List<Group> getGroupsByTraining(Long id) {
        List<Long> groupIds = trainingGroupRepository.getGroupsByTraining(id);
        return groupService.findByIdIn(groupIds);
    }

    @Override
    public List<Instructor> getInstructorsByTraining(Long id) {
        List<Long> instructorIds = trainingGroupRepository.getInstructorsByTraining(id);
        return instructorService.findByIdIn(instructorIds);
    }

    @Override
    public List<Training> getTrainingByGroup(Long id) {
        List<Long> trainingIds = trainingGroupRepository.getTrainingByGroup(id);
        return trainingService.findByIdIn(trainingIds);
    }

    @Override
    public List<Instructor> getInstructorsByGroup(Long id) {
        List<Long> instructorsIds = trainingGroupRepository.getInstructorsByGroup(id);
        return instructorService.findByIdIn(instructorsIds);
    }

    @Override
    public List<Group> getGroupsByInstructor(Long id) {
        List<Long> groupIds = trainingGroupRepository.getGroupsByInstructor(id);
        return groupService.findByIdIn(groupIds);
    }

    @Override
    public List<Training> getTrainingsByInstructor(Long id) {
        List<Long> trainingIds = trainingGroupRepository.getTrainingsByInstructor(id);
        return trainingService.findByIdIn(trainingIds);
    }

    @Override
    public TrainingGroup save(Long trainingId, Long groupId, Long instructorId) {

        TrainingGroup trainingGroup = new TrainingGroup();

        Training training = trainingService.findById(trainingId).get();
        Group group = groupService.findById(groupId).get();
        Instructor instructor = instructorService.findById(instructorId).get();
        Timestamp currentTime = Timestamp.from(Instant.now());

        trainingGroup.setTraining(training);
        trainingGroup.setGroup(group);
        trainingGroup.setInstructor(instructor);
        trainingGroup.setCreatedAt(currentTime);
        trainingGroup.setUpdatedAt(currentTime);

        return trainingGroupRepository.save(trainingGroup);
    }
}
