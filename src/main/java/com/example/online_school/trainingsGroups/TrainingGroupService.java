package com.example.online_school.trainingsGroups;

import com.example.online_school.groups.models.Group;
import com.example.online_school.instructors.models.Instructor;
import com.example.online_school.trainings.models.Training;
import com.example.online_school.trainingsGroups.models.TrainingGroup;

import java.util.List;

public interface TrainingGroupService {

    List<Group> getGroupsByTraining(Long id);

    List<Instructor> getInstructorsByTraining(Long id);

    List<Training> getTrainingByGroup(Long id);

    List<Instructor> getInstructorsByGroup(Long id);

    List<Group> getGroupsByInstructor(Long id);

    List<Training> getTrainingsByInstructor(Long id);

    TrainingGroup save(Long trainingId, Long groupId, Long instructorId);
}
