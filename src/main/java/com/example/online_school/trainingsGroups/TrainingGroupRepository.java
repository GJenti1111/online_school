package com.example.online_school.trainingsGroups;

import com.example.online_school.groups.models.Group;
import com.example.online_school.trainingsGroups.models.TrainingGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface TrainingGroupRepository extends CrudRepository<TrainingGroup, Long> {

    @Query(value = "SELECT group_id from trainings_groups where training_id = :id",nativeQuery = true)
    List<Long> getGroupsByTraining(@PathVariable ("id") Long id);

    @Query(value = "SELECT instructor_id from trainings_groups where training_id = :id",nativeQuery = true)
    List<Long> getInstructorsByTraining(@PathVariable ("id") Long id);

    @Query(value = "SELECT training_id from trainings_groups where group_id = :id",nativeQuery = true)
    List<Long> getTrainingByGroup(@PathVariable ("id") Long id);

    @Query(value = "SELECT instructor_id from trainings_groups where group_id = :id",nativeQuery = true)
    List<Long> getInstructorsByGroup(@PathVariable ("id") Long id);

    @Query(value = "SELECT group_id from trainings_groups where instructor_id = :id",nativeQuery = true)
    List<Long> getGroupsByInstructor(@PathVariable ("id") Long id);

    @Query(value = "SELECT training_id from trainings_groups where instructor_id = :id",nativeQuery = true)
    List<Long> getTrainingsByInstructor(@PathVariable ("id") Long id);
}
