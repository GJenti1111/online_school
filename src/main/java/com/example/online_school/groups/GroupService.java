package com.example.online_school.groups;

import com.example.online_school.groups.models.Group;
import com.example.online_school.groups.models.GroupDto;

import java.util.List;
import java.util.Optional;

public interface GroupService {

    Iterable<Group> findAll();

    Optional<Group> findById(Long id);

    Group save(Group group);

    Group update(Long id, GroupDto groupDto);

    void delete(Group group);

    List<Group> findByIdIn(List<Long> ids);
}
