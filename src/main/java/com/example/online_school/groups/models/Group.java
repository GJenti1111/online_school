package com.example.online_school.groups.models;


import com.example.online_school.commons.BaseModel;
import com.example.online_school.studentsGroups.models.StudentGroup;
import com.example.online_school.trainingsGroups.models.TrainingGroup;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "groups")
@JsonIdentityInfo(generator= ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Group extends BaseModel {

    private String title;
    private Date startAt;
    private Date endAt;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }

    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
    @JsonIgnore
    Set<StudentGroup> studentGroups = new HashSet<>();

    @OneToMany(mappedBy = "group", fetch = FetchType.EAGER)
    @JsonIgnore
    Set<TrainingGroup> trainingGroups = new HashSet<>();

    public Set<StudentGroup> getStudentGroups() {
        return studentGroups;
    }

    public void setStudentGroups(Set<StudentGroup> studentGroups) {
        this.studentGroups = studentGroups;
    }

    public Set<TrainingGroup> getTrainingGroups() {
        return trainingGroups;
    }

    public void setTrainingGroups(Set<TrainingGroup> trainingGroups) {
        this.trainingGroups = trainingGroups;
    }
}
