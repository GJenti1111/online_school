package com.example.online_school.groups.models;

import com.example.online_school.commons.BaseModel;

import java.util.Date;

public class GroupDto extends BaseModel {

    private String title;
    private Date startAt;
    private Date endAt;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEndAt() {
        return endAt;
    }

    public void setEndAt(Date endAt) {
        this.endAt = endAt;
    }
}
