package com.example.online_school.groups;

import com.example.online_school.groups.models.Group;
import com.example.online_school.groups.models.GroupDto;
import com.example.online_school.groups.utils.GroupMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

@Service
public class GroupServiceImpl implements GroupService{

    private GroupRepository groupRepository;
    private GroupMapper groupMapper;

    @Autowired
    public GroupServiceImpl(GroupRepository groupRepository, GroupMapper groupMapper) {
        this.groupRepository = groupRepository;
        this.groupMapper = groupMapper;
    }

    @Override
    public Iterable<Group> findAll() {
        return groupRepository.findAll();
    }

    @Override
    public Optional<Group> findById(Long id) {
        Optional<Group> optionalGroup = groupRepository.findById(id);
        if (optionalGroup.isPresent()) {
            return optionalGroup;
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "the group with id " + id + "is not found");
        }
    }

    @Override
    public Group save(Group group) {
        return groupRepository.save(group);
    }

    @Override
    public Group update(Long id, GroupDto groupDto) {
        Optional<Group> optionalGroup = groupRepository.findById(id);
        if (optionalGroup.isPresent()) {
            Group myGroup = optionalGroup.get();
            groupMapper.updateGroupFromDto(groupDto, myGroup);
            return groupRepository.save(myGroup);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "the group with id " + id + "is not found");
        }
    }

    @Override
    public void delete(Group group) {
        groupRepository.delete(group);
    }

    @Override
    public List<Group> findByIdIn(List<Long> ids) {
        return groupRepository.findByIdIn(ids);
    }
}
