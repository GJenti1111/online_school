package com.example.online_school.groups;

import com.example.online_school.groups.models.Group;
import com.example.online_school.groups.models.GroupDto;
import com.example.online_school.instructors.models.Instructor;
import com.example.online_school.students.models.Student;
import com.example.online_school.studentsGroups.StudentGroupService;
import com.example.online_school.studentsGroups.models.StudentGroup;
import com.example.online_school.trainings.models.Training;
import com.example.online_school.trainingsGroups.TrainingGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/groups")
public class GroupController {

    private GroupService groupService;
    private StudentGroupService studentGroupService;
    private TrainingGroupService trainingGroupService;

    @Autowired
    public GroupController(GroupService groupService, StudentGroupService studentGroupService, TrainingGroupService trainingGroupService) {
        this.groupService = groupService;
        this.studentGroupService = studentGroupService;
        this.trainingGroupService = trainingGroupService;
    }

    @GetMapping
    public Iterable<Group> findAll() {
        return groupService.findAll();
    }

    @GetMapping(path = "/{id}")
    public Optional<Group> findById(@PathVariable Long id) {
        return groupService.findById(id);
    }

    @PostMapping
    public Group save(@RequestBody Group group) {
        return groupService.save(group);
    }

    @PatchMapping(path = "/{id}")
    public Group update(@PathVariable Long id, @RequestBody GroupDto groupDto) {
        return groupService.update(id, groupDto);
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable ("id") Group group) {
        groupService.delete(group);
    }

    @PostMapping(path = "/{groupId}/students/{studentId}")
    public StudentGroup save(@PathVariable Long groupId, @PathVariable Long studentId) {
        return studentGroupService.save(groupId, studentId);
    }

    @GetMapping(path = "/{id}/students")
    public List<Student> getStudentByGroup(@PathVariable Long id) {
        return studentGroupService.getStudentByGroup(id);
    }

    @GetMapping(path = "/{groupId}/students/{studentId}")
    public StudentGroup getSGByBothIds(@PathVariable Long groupId, @PathVariable Long studentId) {
        return studentGroupService.getSGByBothIds(groupId, studentId);
    }

    @DeleteMapping(path = "/{groupId}/students/{studentId}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void deleteStudentGroupByBothIds(@PathVariable Long groupId, @PathVariable Long studentId) {
        studentGroupService.deleteStudentGroupByBothIds(groupId, studentId);
    }

    @PostMapping(path = "{groupId}/students")
    public Iterable<StudentGroup> saveAll(@PathVariable Long groupId, @RequestParam(name = "ids") List<Long> ids) {
        return studentGroupService.saveAll(groupId,ids);
    }

    @GetMapping(path = "/{id}/trainings")
    public List<Training>getTrainingByGroup(@PathVariable Long id) {
        return trainingGroupService.getTrainingByGroup(id);
    }

    @GetMapping(path = "/{id}/instructors")
    public List<Instructor> getInstructorsByGroup(@PathVariable Long id) {
        return trainingGroupService.getInstructorsByGroup(id);
    }
}
