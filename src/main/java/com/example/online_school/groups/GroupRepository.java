package com.example.online_school.groups;

import com.example.online_school.groups.models.Group;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GroupRepository extends CrudRepository<Group, Long> {

    List<Group> findByIdIn(List<Long> ids);
}
